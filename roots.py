import gmpy2
from gmpy2 import mpz

def primeFactors(n):
    res = []

    if gmpy2.is_prime(n):
        res.append(n)
        return res
    cur = 2
    # don't try too long to factor; just give up and try another
    while True:
        if n % cur == 0:
            res.append(cur)
            while n % cur == 0:
                n = n // cur
            if gmpy2.is_prime(n):
                res.append(n)
                return res
        # lol hack to try 2, then only odds after that
        cur += 1 + (cur & 1)

def braceify(arr):
    return "{" + ", ".join(str(a) for a in arr) + "}"

if __name__ == '__main__':
    power = 1 << 27
    cur = (1 << 31) + power + 1
    finds = []
    while len(finds) < 3:
        if gmpy2.is_prime(cur):
            print(cur)
            factors = primeFactors(cur - 1)
            # procedure for checking primitive root of unity, taken from:
            # https://math.stackexchange.com/questions/124408/finding-a-primitive-root-of-a-prime-number
            root = 2
            while True:
                found = True
                for f in factors:
                    if gmpy2.powmod(root, (cur - 1) // f, cur) == 1:
                        found = False
                if found:
                    break
                root += 1
            # convert primitive root to w, s.t. w^power = 1 (mod cur)
            root = gmpy2.powmod(root, cur // power, cur)
            finds.append((cur, root, gmpy2.powmod(root, -1, cur)))
        cur += power
    print(finds)
