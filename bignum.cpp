#include <bits/stdc++.h>
using namespace std;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef unsigned __int128 u128;

class Bignum {
	static const u64 LOW = (1LL << 32) - 1;
	static const u64 HI = LOW << 32;
private:
	void add_shifted(const vector<u32> &other, const u32 shift) {
		assert(other.size() + shift <= data.size());
		bool carry = false;
		for (int i = 0; i < other.size(); i++) {
			u64 cur = (u64)data[i + shift] + carry + other[i];
			data[i + shift] = cur & LOW;
			carry = (cur > LOW);
		}
	}

	static vector<u32> fft(const vector<u32> &a, u32 mod, u32 root) {
		const u32 n = a.size();
		u32 *ws = new u32[n];
		u64 cur = 1;
		for (int i = 0; i < n; i++) {
			ws[i] = cur;
			cur = (cur * root) % mod;
		}
		vector<u32> res = fft_recursive(a, ws, 1, 0, mod);
		free(ws);
		return res;
	}

	static vector<u32> fft_recursive(const vector<u32> &a, const u32 *ws, u32 stride, u32 offset, u32 mod) {
		const u32 n = a.size() / stride;
		if (n == 1) return vector<u32> {a[offset]};
		// if (n < 16) return fft_n2(a, ws, stride, offset, mod);

		vector<u32> even_fft = fft_recursive(a, ws, stride << 1, offset, mod);
		vector<u32> odd_fft = fft_recursive(a, ws, stride << 1, offset + stride, mod);
		vector<u32> res(n);
		for (int i = 0; i < n / 2; i++) {
			u64 odd = ((u64)ws[i * stride] * odd_fft[i]) % mod;
			res[i] = ((u64)even_fft[i] + odd) % mod;
			res[i + n/2] = ((u64)even_fft[i] - odd + mod) % mod;
		}
		return res;
	}

	static vector<u32> fft_n2(const vector<u32> &a, const u32 *ws, u32 stride, u32 offset, u32 mod) {
		const u32 n = a.size() / stride;
		vector<u32> res(n);
		for (int i = 0; i < n; i++) {
			u64 cur = 0;
			u32 idx = 0;
			for (int j = offset; j < a.size(); j += stride) {
				cur = (cur + (u64)ws[idx] * a[j]) % mod;
				idx = (idx + i * stride) & (a.size() - 1);
			}
			res[i] = cur;
		}
		return res;
	}

	static Bignum mult_n2(const Bignum &a, const Bignum &b) {
		Bignum res = mult(a, b.data[0]);
		for (int i = 1; i < b.data.size(); i++) {
			Bignum cur = mult(a, b.data[i]);
			cur.data.insert(cur.data.begin(), i, 0);
			res = add(res, cur);
		}
		return res;
	}

	static Bignum mult_fft(const Bignum &a, const Bignum &b) {
		const u32 rootPow = 27;
		const u32 P[] = {2281701377, 3221225473, 3489660929};
		const u32 W[] = {129140163, 229807484, 1392672017};

		u32 exp = 4, n = max(a.data.size(), b.data.size());
		while ((1 << exp) < 2 * n - 1) exp++;

		// pad our inputs to be the same power-of-two length
		n = (1 << exp);
		vector<u32> v1(a.data), v2(b.data);
		v1.insert(v1.end(), n - v1.size(), 0);
		v2.insert(v2.end(), n - v2.size(), 0);

		vector<u32> mods[3];
		for (int i = 0; i < 3; i++) {
			// w is a 2^exp-th root of unity
			const u32 w = modExp(W[i], 1 << (rootPow - exp), P[i]);
			mods[i] = fft(v1, P[i], w);
			auto bmod = fft(v2, P[i], w);
			for (int j = 0; j < n; j++) {
				u64 m1 = mods[i][j], m2 = bmod[j];
				mods[i][j] = (m1 * m2) % P[i];
			}
			u32 rootInv = modInv(w, P[i]);
			u64 nInv = modInv(n, P[i]);
			mods[i] = fft(mods[i], P[i], rootInv);
			for (auto& cur : mods[i])
				cur = (nInv * (u64)cur) % P[i];
		}
		vector<u32> vres(n + 3);
		Bignum res(vres);

		const u128 tot_prod = (u128)P[0] * P[1] * P[2];
		u128 scale[] = {1, 1, 1};
		for (int i = 0; i < 3; i++) {
			u64 inv = 1;
			for (int j = 0; j < 3; j++) {
				if (i == j) continue;
				inv *= modInv(P[j], P[i]);
				scale[i] *= P[j];
			}
			scale[i] *= (inv % P[i]);
		}

		for (int idx = 0; idx < n; idx++) {
			u128 cur128 = 0;
			for (int i = 0; i < 3; i++) {
				cur128 += (u128)mods[i][idx] * scale[i];
				cur128 %= tot_prod;
			}
			u32 p0 = cur128 & LOW;
			u32 p1 = (cur128 >> 32) & LOW;
			u32 p2 = (cur128 >> 64);
			res.add_shifted(vector<u32>{p0, p1, p2}, idx);
		}
		return res;
	}

	static u32 modExp(u32 base, u32 power, u32 mod) {
		u64 B = base, M = mod, res = 1;
		for (; power; power >>= 1) {
			if (power & 1) res = (res * B) % M;
			B = (B * B) % M;
		}
		return res;
	}

	// inverse modulo p, a prime
	// by FLT, w^(p-1)=1 (mod p) so w^(p-2) is modular inverse
	static inline u32 modInv(u32 num, u32 p) {
		return modExp(num, p - 2, p);
	}

public:
	vector<u32> data;
	Bignum(string s) {
		data = vector<u32>({0});
		for (int i = 0; i < s.size(); i += 9) {
			u32 cur = 0, pow10 = 1;
			for (int j = 0; j < 9 && i + j < s.size(); j++, pow10 *= 10)
				cur = 10 * cur + (s[i + j] - '0');
			data = mult(data, pow10).data;
			data = add(data, cur).data;
		}
	}
	Bignum (vector<u32> data) : data(data) {}
	Bignum (u32 word) : data({word}) {}

	const string to_string() {
		string res = "";
		Bignum tmp(data);
		const u32 divisor = 1e9;
		while (tmp.data.size() > 1 || (tmp.data.size() > 0 && tmp.data[0] > 0)) {
			auto cur = remdiv(tmp, divisor);
			string digits = std::to_string(cur.first);
			reverse(digits.begin(), digits.end());

			tmp = cur.second;
			if (tmp.data.size() > 1 || (tmp.data.size() > 0 && tmp.data[0] > 0))
				while (digits.size() < 9)
					digits += "0";
			res += digits;
		}
		reverse(res.begin(), res.end());
		return res;
	}

	static Bignum mult(const Bignum &a, const Bignum &b) {
		// this should be way bigger but whatever
		const int thresh = 100;
		Bignum res = (a.data.size() > thresh && b.data.size() > thresh) ?
			mult_fft(a, b) : mult_n2(a, b);

		u32 zct = 0;
		while (zct < res.data.size() && res.data[res.data.size() - zct - 1] == 0) zct++;
		res.data.erase(res.data.end() - zct, res.data.end());
		return res;
	}

	static Bignum mult(const Bignum &a, u32 B) {
		u32 carry = 0;
		u64 b = B;
		vector<u32> res(a.data.size() + 1);
		for (int i = 0; i < a.data.size(); i++) {
			u64 cur = b * a.data[i];
			res[i] = (cur + carry) & LOW;
			u32 hi = cur >> 32;
			if ((cur & LOW) + carry > LOW)
				hi++;
			carry = hi;
		}
		if (carry > 0)
			*(res.rbegin()) = carry;
		else
			res.resize(a.data.size());
		return Bignum(res);
	}

	static pair<u32, Bignum> remdiv(const Bignum &a, u32 B) {
		u64 left = 0, b = B;
		vector<u32> res(a.data.size());
		for (int i = a.data.size() - 1; i >= 0; i--) {
			res[i] = (left | a.data[i]) / b;
			left = (left + a.data[i] - res[i] * b) << 32;
		}
		if (*(res.rbegin()) == 0)
			res.resize(res.size() - 1);
		return {left >> 32, Bignum(res)};
	}

	static Bignum add(const Bignum &a, const Bignum &b) {
		if (a.data.size() < b.data.size()) return add(b, a);

		vector<u32> res(a.data.size() + 1);
		bool carry = false;
		for (int i = 0; i < a.data.size(); i++) {
			u64 cur = a.data[i] + carry;
			if (i < b.data.size()) cur += b.data[i];
			res[i] = cur & LOW;
			carry = (cur > LOW);
		}
		if (carry)
			*(res.rbegin()) = 1;
		else
			res.resize(a.data.size());
		return Bignum(res);
	}
};

int main(int argc, char *argv[]) {
	if (argc == 1) {
		Bignum b("15190258094151045811453651392391");
		Bignum b2 = Bignum::mult(b, b);
		string res = "230743940966921362556020066775401207531663755234020293052696881";
		string b2_str = b2.to_string();
		cout << b2_str << '\n' << res <<  '\n' << (res == b2_str) << '\n';
		return 0;
	}
	Bignum a(argv[1]), b(argv[2]);

	// stupid benchmarking crap to prevent mult call from being optimized out
	u64 state = 0;
	for (int i = 0; i < 20; i++)
		state += Bignum::mult(a, b).data.back();
	if (state < 0) return 1;

	cout << Bignum::mult(a, b).to_string() << '\n';
}
