from random import getrandbits
import subprocess

if __name__ == '__main__':
    num_tests = 20
    for i in range(1, num_tests + 1):
        a = getrandbits(240000 + 129 * i)
        b = getrandbits(240000 + 129 * i)
        completed = subprocess.run(["./bignum", str(a), str(b)], capture_output=True)
        result = completed.stdout.decode('utf-8').strip()
        if result == str(a * b):
            print("Passed {}/{}".format(i, num_tests))
        else:
            print("Failed on {} * {}".format(a, b))
