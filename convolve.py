import numpy as np

# assume equal length, power of two length
def convolve_n2(a, b):
    n = len(a)
    b_rev = np.roll(b[::-1], 1)
    res = []
    for i in range(n):
        res.append(np.dot(a, b_rev))
        b_rev = np.roll(b_rev, 1)
    return res

def convolve_k(a, b):
    n = len(a)
    res = _convolve_k(a, b)
    return np.roll(res[:n] + res[n:], -1)

def _convolve_k(a, b):
    n = len(a)
    if n == 1:
        return np.array([0, a * b])
    nhalf = n // 2
    alo, blo = a[:nhalf], b[:nhalf]
    ahi, bhi = a[nhalf:], b[nhalf:]

    convb = _convolve_k(alo, blo)
    convc = _convolve_k(ahi, bhi)
    conva = _convolve_k(alo+ahi, blo+bhi) - convb - convc
    res = np.concatenate([convb, convc])
    res2 = np.concatenate([np.zeros_like(alo), conva, np.zeros_like(alo)])
    return res + res2

if __name__ == '__main__':
    sz = 256
    a = np.random.randint(0, 50, size=sz)
    b = np.random.randint(0, 50, size=sz)
    print("ListConvolve[{}, {}, 1]".format(str(list(a)), str(list((b)))))
    print(convolve_n2(a, b))
    print(list(convolve_k(a, b)))
